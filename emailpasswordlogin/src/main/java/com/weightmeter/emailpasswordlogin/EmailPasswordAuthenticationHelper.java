package com.weightmeter.emailpasswordlogin;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthEmailException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

public class EmailPasswordAuthenticationHelper {

    private FirebaseAuth mAuth;

    private EmailPasswordAuthenticationListener listener;

    public EmailPasswordAuthenticationHelper(EmailPasswordAuthenticationListener listener) {
        this.listener = listener;
        mAuth = FirebaseAuth.getInstance();
    }

    public void performSingIn(String email, String password) {
        if (isUserSignedIn()) {
            listener.onAlreadySignedIn(mAuth.getCurrentUser());
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            listener.onSignInSuccess(mAuth.getCurrentUser(),
                                    task.getResult().getAdditionalUserInfo().isNewUser());
                        } else {
                            // If sign in fails, display a message to the user.
                            listener.onSignInFail(task.getException().getMessage());
                        }
                    }
                });
    }

    public void performSingUp(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            listener.onSignUpSuccess(mAuth.getCurrentUser());
                        } else if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                            listener.onAlreadySignedUp();
                        } else if (task.getException() instanceof FirebaseAuthEmailException) {
                            listener.onInvalidEmail();
                        } else if (task.getException() instanceof FirebaseAuthWeakPasswordException) {
                            listener.onWeakPassword();
                        } else {
                            listener.onSignUpFail(task.getException().getMessage());
                        }
                    }
                });

    }

    public void resetPassword(String email) {
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()) {
                    listener.onSendPasswordResetEmail();
                } else {
                    listener.onPasswordResetFailed();
                }
            }
        });
    }

    public void signOut() {
        mAuth.signOut();
    }

    public boolean isUserSignedIn() {
        return mAuth.getCurrentUser() != null;
    }
}
