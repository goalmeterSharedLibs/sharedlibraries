package com.weightmeter.emailpasswordlogin;

import com.google.firebase.auth.FirebaseUser;

public interface EmailPasswordAuthenticationListener {
        void onSignInFail(String errorMessage);

        void onSignUpFail(String errorMessage);

        void onSignInSuccess(FirebaseUser user, boolean isNewUser);

        void onSignUpSuccess(FirebaseUser user);

        void onInvalidEmail();

        void onWeakPassword();

        void onAlreadySignedIn(FirebaseUser user);

        void onAlreadySignedUp();

        void onSendPasswordResetEmail();

        void onPasswordResetFailed();
}
