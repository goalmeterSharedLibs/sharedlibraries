package com.weightmeter.googleauthentication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class GoogleSignInHelper {

    private final static int RC_SIGN_IN = 1;

    private FirebaseAuth mAuth;
    private GoogleSignInListener listener;
    private GoogleSignInClient signInClient;

    public GoogleSignInHelper(Context context, String webClintId, GoogleSignInListener listener) {
        this.listener = listener;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(webClintId)
                .requestEmail()
                .build();
        this.signInClient = GoogleSignIn.getClient(context, gso);
        this.mAuth = FirebaseAuth.getInstance();
    }

    public GoogleSignInHelper(final Activity activity, SignInButton signInButton, String webClientId, final GoogleSignInListener listener) {
        this(activity, webClientId, listener);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSignIn(activity);
            }
        });

    }

    public void performSignIn(Activity activity) {
        if (isUserSigned()) {
            handlePreSignIn(activity);
            return;
        }

        showSignInActivity(activity);
    }

    private void showSignInActivity(Activity activity) {
        Intent signInIntent = signInClient.getSignInIntent();
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void onActivityResult(int requestCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            GoogleSignInAccount account = getSignInResult(task);
            if (account == null) {
                listener.onFail("Sign in failed");
            } else {
                firebaseAuthWithGoogle(account);
            }
        }
    }

    private void handlePreSignIn(Activity activity) {

        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            listener.onAlreadySignedIn(user);
        } else {
            showSignInActivity(activity);//open login activity if user wasn't sign in
        }
    }

    public void performSignOut() {
        mAuth.signOut();
    }

    public boolean isUserSigned() {
        return mAuth.getCurrentUser() != null;
    }

    /*
    * Prepares account data from Task
    * */
    private GoogleSignInAccount getSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            return account;
        } catch (Exception e) {
            return null;
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            listener.onSuccess(user, task.getResult().getAdditionalUserInfo().isNewUser());
                        } else {
                            listener.onFail(task.getException().getMessage());
                        }
                    }
                });
    }
}
