package com.weightmeter.googleauthentication;

import com.google.firebase.auth.FirebaseUser;

public interface GoogleSignInListener {
    void onFail(String errorMessage);

    void onSuccess(FirebaseUser firebaseUser, boolean isNewUser);

    void onAlreadySignedIn(FirebaseUser firebaseUser);
}
